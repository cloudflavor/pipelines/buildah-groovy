Buildah
----

You can leverage [buildah](https://github.com/projectatomic/buildah) to build
images on your nodes without docker.

Find out more in the [documentation](./docs/buildah.md).  

This still requires root access for buildah.  
