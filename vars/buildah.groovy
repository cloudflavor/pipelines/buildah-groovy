void buildImage(Map args) {
  def dockerfilePath = args.&dockerfile ?: 'Dockerfile'
  def workspace = args.&workspace
  def imageName = args.&imageName
  def imageTag = args.&imageTag

  validateDockerfile(this.dockerfilePath)

  runBuildah(
    actionOptions: "--storage-driver=vfs \
      -t ${imageName}:${imageTag} \
      ${dockerFilePath} .",
  )
}

void withContainer(String containerName, Map containerOptions) {

}

private void validateDockerfile(String dockerFilePath) {
  File dockerFile = new File(dockerFilePath)
  if (!dockerFile.exists()) {
    throw new Exception('Dockerfile not found in specified path') as java.lang.Throwable
  }
}

private void runBuildah(String actionOptions) {
  final String buildahBin = '/usr/bin/buildah'

  sh(
    returnStdout: true,
    script: """${buildahBin} ${buildahOptions} ${operationOptions}
    """,
  )
}

return this
