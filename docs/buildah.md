### Setup

In order for this module to work you need to install buildah according to these  [instructions](https://github.com/projectatomic/buildah/blob/master/install.md).


###### build images
e.g.:
```groovy
buildImage(
  author: 'Cloudflavor Org',
  imageBuildName: 'test-buildah',
  imageTag: 'latest',
  annotations: [
    'buildTime': new Date().format('yy-MM-dd:mm:ss'),
  ]
  label: 'test-image',
)
```
The commands are described in the [buldah man pages](https://github.com/projectatomic/buildah/blob/master/docs/buildah-bud.md).

###### run commands inside a container
```groovy
withContainer(
  containerName: 'test-container',
  imageName: 'alpine:latest',
  commandList: [
    'cat /etc/os-release',
    "echo 'This is a test!'",
  ]
)
```

Caveats:
* Buildah must be run as root till rootless containers are implemented. And for
that, you need to add the jenkins user or jenkins group as a privileged
user/group in you `/etc/sudoers` file like `%jenkins	ALL=(ALL)	NOPASSWD: ALL`.  
This way buildah can be executed, on the node, without a password prompt.  
* buildah binary must be installed in the usual place `/usr/bin/buildah` on
your node
